# Influence on Health by Sportsfields per Neighborhood in Zwolle #

## Do sportsfields in neighborhoods in Zwolle have an influence on several health factors based on data per neighborhood? ##
## Correlations based on open data gathered from RIVM, CBS, and the municipality of Zwolle ##

### Dependencies ###

- yaml (5.3.1)
- glob
- json (2.0.9)
- folium (0.12.1)
- bokeh (2.2.3)
- os.path
- scipy (1.6.0)
- pandas (1.2.0)
- numpy (1.19.5)
- panel (0.10.2)
- holoviews (1.14.1)
- hvplot (0.7.0)
- shapely(1.7.1)

### State the region and the domain category that your data sets are about ###
- Region: Municipality of Zwolle.
- Datasets: 
	* source = RIVM: Dataset containing percentual population data of several health factors per neighborhood (2016)
	* source = CBS: Dataset containing population data in key figures (2016)
	* source = Open Data Hub Zwolle: Info and locations of sports fields in Zwolle
	* source = Open Data Hub Zwolle: Info about neighborhoods in Zwolle
	* source = Open Data Hub Zwolle: Geo-info about neighborhoods in zwolle (coords)

### State the research question ###
- Do sportsfields in neighborhoods of Zwolle influence certain health factors?


### Justify the chosen data storage and processing approach ###
- RIVM (health) data (.csv) can be downloaded from https://statline.rivm.nl/#/RIVM/nl/dataset/50052NED/table?ts=1610006130972 , data from Zwolle needs to be selected by hand using the filter on the website.
- CBS (population) data (.csv) can be downloaded from  https://opendata.cbs.nl/#/CBS/nl/dataset/83487NED/table?ts=1614166061725 , data from Zwolle needs to be selected by hand using the filter on the website.
- SportsFields data (.json) can be downloaded from https://en-smart-zwolle.opendata.arcgis.com/datasets/sportvelden/geoservice?orderBy=BEHEERDER 
- Neighborhood data (.json) can be downloaded from https://smart-zwolle.opendata.arcgis.com/datasets/wijkgrenzen-zwolle/geoservice?geometry=5.454%2C52.443%2C6.760%2C52.589
- Neighborhood (geo)data (.json) can be downloaded from https://opendata.arcgis.com/datasets/79dd20dc3f2e4c0f8fcb4366c536a534_2.geojson

* Data is stored in this repo under the 'data' folder, since the files are small and no privacy will be harmed since it's open data.
* All data is processed and visualized programatically using python with the packages mentiond above.

### Justify the chosen analysis approach ###
- The global approach was as follows: All the data was read according to their datatypes. Data useful to combine was merged together in a df, with columns as variables.
The columns in the df now contain both the health data, and the neighborhood/sportfield data. 
Also some calculations were done to account for the fact that some neighborhoods have a higher population than others.
Negative variables were inversed, so a 'good' influence now should have the same consequences for each variable. 
Finally some outliers were excluded for the analysis part, e.g. neighborhoods that didn't contain any sportsfields.
The main analysis part was done by comparing columns with each other, to see if there were correlations between them.
Comparing columns was done by creating scatterplots of them using a panel interaction 'dashboard'. 
Also a heatmap was also made to see all correlations between variables in one eyesight. 

### Justify the chosen data visualization approach ###
- A map was plotted with all neighborhoods. When you hover over the neighborhoods you'll see their number and the total area of said neighborhood.
The locations of the sportsfields are also indicated as clusters, when zoomed in, the exact locations can be seen. 
In a dropdown menu the variable can be selected on which the neighborhoods will be colored. Legend can be seen top-right in the plot.
This map was mainly made for exploration of the data, since no real correlations can be seen using this plot. 
But it is handy to see the distribution of the variables per neighborhood quite easily; only a variable needs to be selected from a menu. 
- Secondly a panel with the mentioned scatterplots was made. In this panel you can select the x-axis from a dropdown menu, which contains the logical variables to put on the x-axis.
Then the y-axis can also be manually selected from a dropdown menu which contains the dependent variables. 
A linear regression line is added to see the trend of the data.
Finally, the color of the datapoints and the legend can also be set using a dropdown menu containing all variables. 
This is handy so you can see the interaction of not two, but even 3 variables in the same graph, to see e.g. if two dependent variables follow the same trend in the plot. 
By default this is set on 'neighborhood', to act as a legend to see which dot represents which neighborhood. 
- Next, as mentioned, a heatmap can be seen to indicate the correlations of all variables at the same time. 
Just as the scatterplots this is based on visual interpretation. The more yellow the boxes, the higher the correlation. 
This doesn't however show if the correlation is negative or positive, and how significant the correlation is. 
- Finally, a dataframe only containing significant correlations was created. 
The first column contains the independent variables, the second column shows the dependent variable which has a correlation with the first column.
In the 3rd column the P-value of the correlation is stated. As it only contains significant correlations, this column contains values < 0.05.
In the 4th column the R-value can be found. This number indicates how 'spread' the data is, and if the correlation is positive or negative (-).